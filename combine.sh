#!/bin/bash

FAHRZEUGE="lf10 lf20 dlk rw1 tsf-w elw mtf"
STATUS="1 2 3 4 6"

for f in $FAHRZEUGE; do
    for s in $STATUS; do
        composite -gravity center status$s.png $f.png $f-status$s.png
        mogrify -resize 30% $f-status$s.png
        convert $f.png -resize 30% $f-k.png 
    done
done
